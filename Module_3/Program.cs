﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
           
        }

    }

   
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int num=0;
            if (!Int32.TryParse(source, out num))
            {
                throw new ArgumentException();
            }
            else
                return num;
        }

        public int Multiplication(int num1, int num2)
        {
            if (num1 == 0 || num2==0)
            {
                return 0;
            }
            int result=0;
            bool minus = false;
            if(num1<=0)
            {
                minus= true;
                string num1S = num1.ToString()[1..];
                num1 = Int32.Parse(num1S);
            }
            for (int i = 1; i <= num1; i++)
            {
                result += num2;
            }
            if(minus)
            {
                if (result.ToString()[0] == '-')
                {
                    return Int32.Parse(result.ToString()[1..]);
                }
                else
                {
                    return Int32.Parse('-' + result.ToString());
                }
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while(true)
            {
                if(Int32.TryParse(input,out result) && result>=0)
                {
                    
                    return true;
                }
                else
                {
                    Console.WriteLine("Try again");
                    input = Console.ReadLine();
                }
            }

            
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> ls = new List<int>();
            for (int i = 0; i <= naturalNumber; i = 2)
            {
                ls.Add(i);
            }
            return ls;

        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (true)
            {
                if (Int32.TryParse(input, out result) && result >= 0)
                {

                    return true;
                }
                else
                {
                    Console.WriteLine("Try again");
                    input = Console.ReadLine();
                }
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string result = source.ToString();
            for(int i =0;i<result.Length;i++)
            {
                if(result[i]==digitToRemove.ToString()[0])
                {
                    result = result.Remove(i);
                    i--;
                }
            }
            return result;
        }
    }
}
